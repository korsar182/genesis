provider "aws" {
  # use export AWS_ACCESS_KEY_ID=’accesskey’ and
  # export AWS_SECRET_ACCESS_KEY=’secretkey’ to set up credentials

  region = "eu-central-1"
}

terraform {
  backend "s3" {
    bucket = "genesis"           // Bucket where to SAVE Terraform State
    key    = "terraform.tfstate" // Object name in the bucket to SAVE Terraform State
    region = "eu-central-1"      // Region where bycket created
  }
}

data "aws_ami" "latest_ubuntu" {
  owners      = ["099720109477"]
  most_recent = true
  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }
}

resource "aws_instance" "web-server" {
  ami                    = data.aws_ami.latest_ubuntu.id
  instance_type          = "t2.micro"
  vpc_security_group_ids = [aws_security_group.webserver.id]
  user_data              = <<-EOF
                  #!/bin/bash
                  echo "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIF/BpMg8v6OKgXocxjLNLdN+uGMjBmWTcHsQaMEXvgh9" >> /home/ubuntu/.ssh/authorized_keys
                  EOF
  tags = {
    Name = "web-server"
  }
  provisioner "local-exec" {
    command = "echo 'web-server      ansible_host=${aws_instance.web-server.public_ip}' >> ../ansible/hosts"
  }
}

resource "aws_instance" "mysql" {
  ami                    = data.aws_ami.latest_ubuntu.id
  instance_type          = "t2.micro"
  vpc_security_group_ids = [aws_security_group.mysql.id]
  user_data              = <<-EOF
                  #!/bin/bash
                  echo "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIF/BpMg8v6OKgXocxjLNLdN+uGMjBmWTcHsQaMEXvgh9" >> /home/ubuntu/.ssh/authorized_keys
                  EOF
  tags = {
    Name = "mysql server"
  }
  provisioner "local-exec" {
    command = "echo 'mysql      ansible_host=${aws_instance.mysql.public_ip}' >> ../ansible/hosts"
  }
}

resource "aws_security_group" "webserver" {
  name        = "WebServer Security Group"
  description = "web-server"

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "web-server"
  }
}

resource "aws_security_group" "mysql" {
  name        = "Mysql Security Group"
  description = "mysql"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 3306
    to_port     = 3306
    protocol    = "tcp"
    cidr_blocks = ["172.31.0.0/16"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "mysql"
  }
}

output "webserver_ip" {
  value = ["${aws_instance.web-server.*.public_ip}"]
}

output "mysql_ip" {
  value = ["${aws_instance.mysql.*.public_ip}"]
}
